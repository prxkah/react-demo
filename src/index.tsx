import React from "react";
import ReactDOM from "react-dom";
import "./index.css";

const App = () => <h1 className="container">Welcome</h1>;

ReactDOM.render(<App />, document.getElementById("root"));
